import express from 'express'
import {name} from './app'

export let router = express.Router()

router.get('/',(req,res)=>{
    res.json('hello from ' + name)
})