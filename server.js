let express = require('express')
// require('ts-node/register')
let lib = require('./router')

let app = express()
app.use(lib.router)

let PORT = 8100
app.listen(PORT,()=>{
    console.log(`http://localhost:${PORT}`);
})